/*Criar uma função que dado um valor indefinido de arrays,
 faça a soma de seus valores
 
 ex: [1,2,3] [1,2,2] [1,1] = 13
     [1,1] [2,20] = 24
*/

function somaEmArrays(listas) {
    var somas = 0;
    for (i in listas){
        var inicio = listas[i]
        for (a in inicio){
            somas += inicio[a]
        }
    }
    console.log(somas)
}

somaEmArrays([[1,2,3],[1,2,2],[1,1]])