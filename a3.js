/*Crie uma função que dada uma strig A e um array de strings,
retorne um array novo com apenas as strings do array que são 
compostas exclusivamente por caracteres da string A;

ex: ('ab', ['abc','ba','ab','bb','kb']) = ['ba', 'ab', 'bb']
'pkt' ['pkt','pp','pata','po','kkkkk'] = ['pkt','pp','kkk']
*/

function reconheceString(str, array){
    novaString = []

    var novoArray = array.filter((e) => {
        var ok = true
        
        for (i in e){
            if(!(str.includes(e[i])) ){
                ok = false
            }
        }
    
        if (ok === true){
            return e
        }
    })

    console.log(novoArray)
}

reconheceString('ab', ['abc','ba','ab','bb','kb'])