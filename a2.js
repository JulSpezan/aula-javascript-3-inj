/*Criar uma função que dado um número n e um array, 
retorne um novo array com os valores do array anterior * n
ex: (2, [1,3,6,10]) = [2,6,12,20]  */

function multiplicaArray(n, lista){
    novo = []
    for (i in lista){
        a = lista[i]
        b = a * n
        novo.push(b)
    }
    console.log(novo)
}


var num = 8
var array = [1,3,6,10]
multiplicaArray(num, array)
