/*Criar uma função que dada n arrays, retorne um novo 
array que possua apenas os valores que existam em todos os n arrays 
ex: [1,2,3],[3,3,7],[9,11,3] = [3]
    [120,120,110,2],[110,2,130] = [110,2]
*/

function reconheceNums(array){
    novaArray = []
    
    first = array[0]
    tam = array.length
    
    for (a = 0; a < first.length; a++) {
        var atual = first[a] 

        for(b = 1; b < tam; b++){
            
            var elem = array[b]

            if (elem.indexOf(atual) != -1){
                novaArray.push(atual)
            }
            else{
                continue
            }
        }
    }
    
    var nova = []
    
    for (i in novaArray){
        valor = novaArray[i]
        
        if(nova.indexOf(valor) == -1){
            nova.push(valor)
        }
        else{
            continue
        }
    }console.log(nova)

}
var lista = [[1,2,3],[3,3,7],[9,11,3]]
reconheceNums(lista)

